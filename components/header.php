<?php require('app/config.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    
    <title>Agencia de Marketing Digital en Cancún | Creativo APP</title>
    <meta name="keywords" content="Agencia de Marketing Digital en Cancún, Agencia de diseño gráfico en Cancún, Posicionamiento web en Cancún, Desarrollo web en Cancún, Desarrollo de aplicaciones móviles en Cancún, Marketing en redes sociales"/>
    <meta name="description" content="Agencia de Marketing Digital en Cancún. Potenciamos tu marca con estrategias de diseño gráfico, posicionamiento web, páginas web, aplicaciones móviles y redes sociales."/>
	
	<meta name="viewport" content="width=device-width, user-scalable=no" />

	<link rel="shortcut icon" type="image/png" href="<?=_IMG.'favicon.ico';?>"/>
	
	<!-- STYLES -->
    <link rel="stylesheet" href="<?=_CSS.'main.min.css';?>">

    <script src="https://kit.fontawesome.com/94233a6903.js" crossorigin="anonymous"></script>

	<!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body>    
    <header class="is-header <?=$pageUri == 'index.php' ? 'is-transparent' : 'is-solid';?>">
        <div class="container">
            <div class="columns">

                <div class="column is-one-third is-brand">
                    <?php $timg = $pageUri == 'index.php' ? '-white' : null; ?>
                    <a href="/"><img src="<?=_IMG.'creativo-app'.$timg.'.png';?>" title="Creativo APP - Marketing, Diseño y Software"></a>
                    <i class="fas fa-bars is-hidden" id="mobile-nav"></i>
                </div>

                <div class="column is-two-thirds is-menu">
                    <ul>
                        <li><a href="/" <?php if($pageUri=='index.php'){ ?> class="is-current" <?php } ?> >Inicio</a></li>
                        <li><a href="/nosotros" <?php if($pageUri=='nosotros.php'){ ?> class="is-current" <?php } ?> >Nosotros</a></li>
                        <li>
                            <a href="/marketing-digital-en-cancun" <?php if($pageUri=='marketing-digital-en-cancun.php'){ ?> class="is-current" <?php } ?>>Servicios</a>
                            <ul class="is-level">
                                <li><a href="/diseno-grafico-web"><i class="fas fa-palette"></i> Diseño Gráfico</a></li>
                                <li><a href="/seo-posicionamiento-web"><i class="fas fa-search-location"></i> Posicionamiento Web</a></li>
                                <li><a href="/desarrollo-paginas-web"><i class="fas fa-laptop-code"></i> Páginas Web</a></li>
                                <li><a href="/aplicaciones-moviles"><i class="fas fa-mobile-alt"></i> Aplicaciones Móviles</a></li>
                                <li><a href="/redes-sociales"><i class="fab fa-facebook-f"></i> Redes Sociales</a></li>
                            </ul>
                        </li>
                        <li><a href="/experiencias" <?php if($pageUri=='experiencias.php'){ ?> class="is-current" <?php } ?>>Experiencias</a></li>
                        <li><a href="/contacto" <?php if($pageUri=='contacto.php'){ ?> class="is-current" <?php } ?>>Contacto</a></li>
                        <li class="is-button"><a href="/cotizar">Cotiza tu Proyecto</a></li>
                        <li class="is-social"><a href="#" target="_blank" class="is-facebook" title="Síguenos en Facebook"><i class="fab fa-facebook"></i></a></li>
                        <li class="is-social"><a href="https://www.instagram.com/cun_creativoapp/" target="_blank" class="is-instagram" title="Síguenos en Instagram"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                </div>

            </div>
        </div>
    </header>