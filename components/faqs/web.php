<div class="is-questions">
    <div class="is-item">
        <div class="is-question">
            <strong>¿Hacen páginas web con Wordpress?</strong>
            <i class="fas fa-chevron-up"></i>
        </div>
        <div class="is-answer is-default">
            <p>Si, porsupuesto pero siempre va a ser así cuando un cliente lo pide específicamente.<br>
            Nuestros desarrollos son propios no utilizamos plantillas o software pre-fabricado, nos tomamos enserio el proceso de crear algo único y especial para ti.</p>
        </div>
    </div>
    
    <div class="is-item">
        <div class="is-question">
            <strong>¿Es alto el costo del Software a la medida?</strong>
            <i class="fas fa-chevron-down"></i>
        </div>
        <div class="is-answer">
            <p>El costo del desarrollo siempre va a depender las características de tu desarrollo.<br>
            Sin embargo; No, no es alto el costo cuando haces un análisis detallado de lo que pierdes sobrecargando o limitando procesos usando soluciones que tienen de más o no tienen lo que necesitas.</p>
        </div>
    </div>
                    
    
</div>