
    <footer class="is-footer">
        <section class="hero-body">
            
            <div class="container">
                <div class="columns is-multiline">
                    
                    <div class="column is-full has-text-centered mb-6">
                        <a href="/"><img src="<?=_IMG.'creativo-app-black.png';?>" title="Creativo APP - Marketing, Diseño y Software" class="is-img-big"></a>
                        <strong>¿Necesitas transformar tu negocio?</strong>
                        <p>Permitele a Creativo App acompañarte en el camino de la transformación de tu negocio, seguro tenemos una solución ideal para ti.</p>
                        <a href="/cotizar" title="Cotiza tu proyecto" class="button is-rounded is-link is-medium">Contactános</a>
                    </div>

                    <div class="column is-three-quarters is-full-desktop has-text-centered-desktop is-full-touch has-text-centered-touch">
                        <ul>
                            <li><a href="/nosotros" title="Acerca de Nosotros">Creativo App</a></li>
                            <li><a href="/paginas-web" title="Desarrollo de Páginas Web">Páginas Web</a></li>
                            <li><a href="/diseno-web" title="Diseño Web">Diseño Web</a></li>
                            <li><a href="/software" title="Desarrollo de Software">Desarrollo de Software</a></li>
                            <li><a href="/apps" title="Aplicaciones Móviles">Aplicaciones Móviles</a></li>
                            <li><a href="/experiencias" title="Nuestros clientes">Clientes</a></li>
                            <li><a href="/cotizar" title="Cotizar Proyecto">Cotizar</a></li>
                            <li><a href="/contacto" title="Contactános">Contacto</a></li>
                            <li><a href="/blog/" title="Blog">Blog</a></li>
                        </ul>
                    </div>

                    <div class="column is-one-quarter has-text-right social is-full-desktop has-text-centered-desktop ">
                        <a href="#" target="_blank" class="is-facebook" title="Síguenos en Facebook"><i class="fab fa-facebook"></i></a>
                        <a href="https://www.instagram.com/cun_creativoapp/" target="_blank" class="is-instagram" title="Síguenos en Instagram"><i class="fab fa-instagram"></i></a>
                    </div>  

                    <div class="pie column is-full">
                        <div class="level">
                            <div class="level-left">
                                <div class="level-item">
                                    <small>Todos los derechos reservados. © Creativo APP <?=date('Y');?></small>    
                                </div>                        
                            </div>
                            <div class="level-right">
                                <div class="level-item">
                                    <small>hola@creativoapp.com</small>
                                </div>
                            </div>
                        </div>
                    </div>      
                </div>

                </div>          

            </div>

        </section>
    </footer>
    

    <script type="text/javascript" src="<?=_JS.'jquery-1.9.0.min.js';?>"></script>
    <script type="text/javascript" src="<?=_JS.'components.js';?>"></script>
	<script type="text/javascript" src="<?=_JS.'ca.js';?>"></script>

</body>
</html>