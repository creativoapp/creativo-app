<div class="container is-contact">
        <div class="columns is-variable is-4">

            <div class="column is-half">
                
                <form name="formGlobalService" method="post" action="" class="columns is-multiline">
                    <fieldset class="column is-full">
                        <label for="inpName">Nombre</label>
                        <input type="text" name="inpName" id="inpName">
                    </fieldset>
                    <fieldset class="column is-half">
                        <label for="inpEmail">Email</label>
                        <input type="text" name="inpEmail" id="inpEmail">
                    </fieldset>
                    <fieldset class="column is-half">
                        <label for="inpNumber">Teléfono</label>
                        <input type="text" name="inpNumber" id="inpNumber">
                    </fieldset>
                    <fieldset class="column is-full">
                        <label for="inpComments">Describe brevemente tu proyecto</label>
                        <textarea name="inpComments" id="inpComments" rows="5"></textarea>
                    </fieldset>
                    <fieldset class="column is-full">
                        <button>CONTACTAR <i class="fas fa-arrow-right"></i></button>
                    </fieldset>
                </form>

            </div>
            
            <div class="column is-half">
                <h3>Realicemos tu proyecto</h3>

                <p class="is-pr-big">Déjanos conocer el objetivo detrás de tu marca. Una vez que contemos con los detalles, podremos maximizar el impacto y los resultados de tu negocio.</p>
                <p class="is-pr-big"><strong>Estamos listos para trabajar contigo.</strong></p>

                <?php 
                    $faqs = null;
                    switch($pageUri) {
                        case 'seo-posicionamiento-web.php':
                            $faqs = 'seo';
                            break;
                        case 'diseno-grafico-web.php':
                            $faqs = 'diseno';
                            break;
                        case 'desarrollo-paginas-web.php':
                            $faqs = 'web';
                            break;
                        case 'aplicaciones-moviles.php':
                            $faqs = 'apps';
                            break;
                        case 'redes-sociales.php':
                            $faqs = 'redes';
                            break;
                    }
                    
                    include __DIR__ . '/faqs/'.$faqs.'.php';

                ?>

            </div>

        </div>
    </div>