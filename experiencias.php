<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-clients">
    <div class="container">
        <div class="columns is-multiline">

            <div class="column is-full">
                <h1>Nuestros Clientes</h1>
                <p class="is-pr-medium">Colaborar en los proyectos de nuestros clientes, más que un servicio, es toda una experiencia. Estas son las marcas que, satisfactoriamente, se han puesto en las manos de nuestra <strong>agencia de marketing digital en Cancún</strong>, Creativo APP. </p>
            </div>

        </div>
    </div>


    <!--TABS-->
    <div class="container is-tabs-clients">
        
        <div class="columns is-tabs">

            <div class="column">
                <a href="seo">Posicionamiento Web</a>
            </div>

            <div class="column">
                <a href="campaigns">Diseño Gráfico</a>
            </div>

            <div class="column">
                <a href="web">Desarrollo Web</a>
            </div>

            <div class="column">
                <a href="graphic">Apps Móviles</a>
            </div>

            <div class="column">
                <a href="social">Redes Sociales</a>
            </div>

        </div>


        <div class="columns is-multiline is-body">

            <div class="column is-one-quarter is-web is-campaigns">
                <img src="<?=_IMG.'projects/desarrollo.jpg';?>" class="is-img-spaced">
                <h2>Bekare Transfers</h2>
                <a href="#">Desarrollo Web</a>, <a href="#">Gestión de Camapañas</a>
            </div>

            <div class="column is-one-quarter is-seo">
                <img src="<?=_IMG.'projects/seo.jpg';?>" class="is-img-spaced">
                <h2>Cliente SEO</h2>
                <a href="#">Posicionamiento Web</a>
            </div>

            <div class="column is-one-quarter is-campaigns is-graphic">
                <img src="<?=_IMG.'projects/desarrollo.jpg';?>" class="is-img-spaced">
                <h2>Cliente DISEÑO</h2>
                <a href="#">Gestión de Campañas</a>, <a href="#">Diseño Gráfico</a>
            </div>

            <div class="column is-one-quarter is-social">
                <img src="<?=_IMG.'projects/seo.jpg';?>" class="is-img-spaced">
                <h2>Cliente REDES</h2>
                <a href="#">Redes Sociales</a>
            </div>

        </div>

    </div>

</section>

<?php require __DIR__ . '/components/footer.php'; ?>