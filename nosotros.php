<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-about">
    <div class="container">
        
        <div class="columns is-multiline">

            <div class="column is-half">
                <h1>Creativo APP</h1>
                <p class="is-pr-medium">Somos una Agencia de <strong>Marketing Digital en Cancún</strong> enfocados, comprometidos y especializados en el desarrollo de soluciones de software.</p>
                <p class="is-pr-medium">Sabemos que tu marca tiene una razón de ser, y nuestro enfoque está en alcanzar tus objetivos.</p>
                <p class="is-pr-medium">¿Cómo lo hacemos? No solamente te oímos, "deveras" te escuchamos. Nos interesamos por el origen y el destino de tu proyecto; analizamos y proponemos las estrategias adecuadas para su evolución, y las llevamos a cabo. A partir de esto, verás un incremento en la visibilidad de tu marca, y por ende, en los frutos que rinda.</p>
                <p class="is-pr-medium">Somos un equipo joven y creativo, con más de cinco años de experiencia. Nuestra ubicación geográfica es Cancún; sin embargo, nuestros clientes se extienden a toda la República Mexicana.</p>
                <p class="is-pr-medium">Dáte la oportunidad de descubrir el verdadero potencial de tu marca y permitele llegar a cada uno de sus objetivos con las estrategias digitales correctas y precisas, en <strong>Creativo APP</strong> queremos formar parte de tu éxito.</p>
                <p class="is-pr-medium">Solicita una <a href="/cotiza">Cotización</a> sin costo.</p>
            </div>

            <div class="column is-half">
                <img src="<?=_IMG.'nosotros-il.png';?>">
            </div>

            <div class="column is-full is-what-do">
                <h3>¿Qué hacemos?</h3>
            </div>

            <div class="column is-one-fifth is-card is-card-service">
                <div>
                    <h2><a href="/diseno-grafico-web"><span>Diseño</span> Gráfico</a></h2>
                    <p>Cuidamos cada uno de los detalles de tu comunicación gráfica de modo en que tu cliente perciba la esencia y el valor de tu marca a través de una imagen profesional.</p>
                    <a href="/diseno-grafico-web" class="is-link" title="Diseño Gráfico y Web en Cancún">Saber más <i class="fas fa-arrow-right"></i></a>
                    <i class="fas fa-palette is-pattern"></i>
                </div>
            </div>

            <div class="column is-one-fifth is-card is-card-service">
                <div>
                    <h2><a href="/seo-posicionamiento-web"><span>Posicionamiento</span> Web</a></h2>
                    <p>Aumentamos el tráfico de tu página web al posicionarlo en los mejores resultados de los buscadores. Además, favorecemos la conversión de visitantes a prospectos al ofrecerles el contenido que están buscando.</p>
                    <a href="/seo-posicionamiento-web" class="is-link" title="Posicionamiento Web en Cancún">Saber más <i class="fas fa-arrow-right"></i></a>
                    <i class="fas fa-search-location is-pattern"></i>
                </div>
            </div>

            <div class="column is-one-fifth is-card is-card-service">
                <div>
                    <h2><a href="/desarrollo-paginas-web"><span>Páginas</span> Web</a></h2>
                    <p>Diseñamos y creamos <strong>Páginas Web</strong> creativas, intuitivas y optimizadas. <strong>¡Queremos que tu marca transmita sus mensajes con éxito!.</strong></p>
                    <a href="/desarrollo-paginas-web" class="is-link" title="Páginas Web en Cancún">Saber más <i class="fas fa-arrow-right"></i></a>
                    <i class="fas fa-laptop-code is-pattern"></i>
                </div> 
            </div>
            
            <div class="column is-one-fifth is-card is-card-service">
                <div>
                    <h2><a href="/aplicaciones-moviles"><span>Apps</span> Móviles</a></h2>
                    <p>Desarrollamos <strong>Aplicaciones Móviles</strong> para resolver las necesidades de tus clientes al momento y de la manera más práctica y cómoda.</p>
                    <a href="/aplicaciones-moviles" class="is-link" title="Aplicaciones Móviles en Cancún">Saber más <i class="fas fa-arrow-right"></i></a>
                    <i class="fas fa-mobile-alt is-pattern"></i>
                </div>
            </div>

            <div class="column is-one-fifth is-card is-card-service">
                <div>
                    <h2><a href="/redes-sociales"><span>Redes</span> Sociales</a></h2>
                    <p>Te ayudamos a diseñar una estrategia de <strong>marketing en redes sociales</strong> que se adapte a las necesidades digitales de tu negocio para aumentar tu presencia, interacción y competitividad en las diferentes plataformas.</p>
                    <a href="/redes-sociales" class="is-link" title="Administración de Redes Sociales en Cancún">Saber más <i class="fas fa-arrow-right"></i></a>
                    <i class="fab fa-facebook is-pattern"></i>
                </div>
            </div>

        </div>
    </div>

    <!--MISION-VISION-->
    <div class="is-vision">
        <div class="container">
            <div class="columns">

                <div class="column is-half">
                    <h2>Visión</h2>
                    <p class="is-pr-big">Creemos firmemente que somos capaces de aportarle a la sociedad un medio más para que las marcas consigan sus objetivos, crezcan y lleguen a alcanzar lo que ellos se definan como exito.</p>

                    <h2>Misión</h2>
                    <p class="is-pr-big">Ayudar incondicionalmente a cada uno de nuestros clientes a lograr sus objetivos a través de nuestros servicios digitales.</p>

                </div>

                <div class="column is-half">
                    <img src="<?=_IMG.'mision-il.png';?>">
                </div>

            </div>
        </div>
    </div>


    <!--CLIENTES-->
    <div class="is-clients">
        <div class="container">
            <div class="columns is-multiline">

                <div class="column is-half">
                
                    <div class="owl-carousel is-ratingg">

                        <div class="item">
                            <strong>Pioneros Cancún FC</strong>
                            <p>Nuestra página web es muy fresca, actual y podemos administrarla con muchisima facilidad, sin duda son una excelente opción.</p>

                            <div class="rating">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                        </div>

                        <div class="item">
                            <strong>Bekare Transfer</strong>
                            <p>Contar con software a la medida para mi negocio es vital, y aquí encontre la forma volver mis necesidades en software de calidad. Muy contento con CreativoAPP.</p>

                            <div class="rating">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                        </div>

                        <div class="item">
                            <strong>Tulum Siankaan Tours</strong>
                            <p>No lograbamos aterrizar de forma exitosa la idea de nuestro logo y sin duda creativoAPP nos dio las propuestas perfectas para darle la mejor identidad posible a nuestra marca.</p>

                            <div class="rating">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                        </div> 

                        <div class="item">
                            <strong>Vuela Viajero</strong>
                            <p>Llegue con mi sitio web infectado con consecuencias en Google y aquí me ayudaron a tener una mejor imagen y limpiar mi presencia en Google, estamos muy contentos.</p>

                            <div class="rating">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                        </div> 

                        <div class="item">
                            <strong>Buffete Jurídico Frías</strong>
                            <p>Me gusto mucho el diseño de mi página web, los tiempos de respuesta y la forma de explicarme su funcionamiento. Muy recomendables.</p>

                            <div class="rating">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                        </div>
                    
                    </div>
                
                </div>


                <div class="column is-half">
                    <h3>Todos somos Creativo APP</h3>
                
                    <div class="columns is-multiline">
                        <div class="column is-one-third is-item">
                            <img src="<?=_IMG.'clients/sun-and-sands.png';?>" class="is-img-big is-img-centered">
                        </div>
                        <div class="column is-one-third is-item">
                            <img src="<?=_IMG.'clients/tulum-sian-kaan-tours.png';?>" class="is-img-big is-img-centered">
                        </div>
                        <div class="column is-one-third is-item">
                            <img src="<?=_IMG.'clients/ruben-frias.png';?>" class="is-img-big is-img-centered">
                        </div>
                        <div class="column is-one-third is-item">
                            <img src="<?=_IMG.'clients/pioneros-cancun.png';?>" class="is-img-big is-img-centered">
                        </div>
                        <div class="column is-one-third is-item">
                            <img src="<?=_IMG.'clients/vuela-viajero.png';?>" class="is-img-big is-img-centered">
                        </div> 
                    </div>

                </div>               

            </div>

        </div>
    </div>

</section>

<?php require __DIR__ . '/components/footer.php'; ?>