<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-servicedetail">
    
    <div class="container">
        <div class="columns">

            <div class="column is-half">
                <h1><span>Posicionamiento</span> Web</h1>
                <div class="is-breads">
                    <?php include __DIR__ . '/components/services-breads.php'; ?>
                    <span>Posicionamiento Web / Seo</span>
                </div>

                <p class="is-pr-medium">Ponemos a tus órdenes nuestros servicios de <strong>posicionamiento web en Cancún</strong>.</p>
                <p class="is-pr-medium">Internet es un mundo infinito que arroja miles de resultados cada vez que realizamos una búsqueda, y dentro de todas estas opciones, tu página web podría pasar desapercibida.</p>
                <p class="is-pr-medium">El <strong>posicionamiento web</strong>, también conocido como SEO (Search Engine Optimization), es una estrategia digital que se emplea para <strong>aumentar la visibilidad de tu sitio web</strong> en los resultados de las búsquedas que realizan los usuarios en buscadores de internet como Google, Yahoo, Bing, entre otros.</p>
                <p class="is-pr-medium">Platícanos qué es lo que quieres promover a través de tu página web, con lo que desarrollaremos contenido de valor que resuelva las necesidades de tus visitantes y que emplee estrategias que permitirán que tu página web se coloque dentro de los principales resultados de quienes te están buscando.</p>
            </div>
            
            <div class="column is-half">
                <img src="<?=_IMG.'seo-il.png';?>" class="is-img">
            </div>

        </div>

        <div class="columns is-multiline is-sub-services">

            <div class="column is-one-quarter">
                <div class="is-item">
                    <h3><i class="fas fa-ad" aria-hidden="true"></i> Keyword Planner</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                </div>
            </div>

            <div class="column is-one-quarter">
                <div class="is-item">
                    <h3><i class="fas fa-wrench"></i> Optimización</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                </div>
            </div>

            <div class="column is-one-quarter">
                <div class="is-item">
                    <h3><i class="fas fa-link" aria-hidden="true"></i> Linkbuilding</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                </div>
            </div>

            <div class="column is-one-quarter">
                <div class="is-item">
                    <h3><i class="fab fa-wordpress-simple" aria-hidden="true"></i> Artículos de Blog</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                </div>
            </div>

        </div>

    </div>
    

    <div class="is-steps">
        <div class="container">
            <div class="columns">

                <div class="column is-one-third">
                    <h3>¿Cómo trabajamos?</h3>
                    <p class="is-pr-big">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    <img src="<?=_IMG.'proceso-il.png';?>">
                </div>

                <div class="column is-two-thirds">
                    <div class="columns is-multiline">
                
                        <div class="column is-half">
                            <div class="is-item">
                                <h4>1. Enserio te escuchamos.</h4>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>
                        </div>

                        <div class="column is-half">
                            <div class="is-item">
                                <h4>2. Te estudiamos a detalle.</h4>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>
                        </div>

                        <div class="column is-half">
                            <div class="is-item">
                                <h4>3. Nos ponemos creativos.</h4>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>
                        </div>

                        <div class="column is-half">
                            <div class="is-item">
                                <h4>4. Tus propuestas.</h4>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>   
                        </div>

                        <div class="column is-half">
                            <div class="is-item">
                                <h4>5. Deleita a tus clientes.</h4>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <?php require __DIR__ . '/components/contact-service.php'; ?>
    
</section>

<?php require __DIR__ . '/components/footer.php'; ?>