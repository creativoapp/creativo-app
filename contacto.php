<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-contact">
    <div class="container">
        <div class="columns is-multiline">

            <div class="column is-half">
                <h1>Contáctanos</h1>
                <p class="is-pr-big">Acercate y cuéntanos sobre tu proyecto, tenemos mucho deseo de ayudarte y contar una nueva experiencia de exito.</p>
            </div>

            <div class="column is-card">
                <div>
                    <strong><i class="fas fa-envelope-open-text"></i>Escríbenos</strong>
                    <a href="mailto:hola@creativoapp.com">hola@creativoapp.com</a>
                </div>
            </div>

            <div class="column is-card">
                <div>
                    <strong><i class="fas fa-fax" aria-hidden="true"></i>Llámanos</strong>
                    <a href="tel:9982235933">(998) 223 5933</a>
                </div>
            </div>

        </div>
    </div>


    <div class="container is-form">
        <div class="columns is-variable">
                
            <div class="column is-half">
                <h4>Queremos conocer tu proyecto</h4>
                <form name="formGlobalService" method="post" action="" class="columns is-multiline">
                    <fieldset class="column is-full">
                        <label for="inpName">Nombre</label>
                        <input type="text" name="inpName" id="inpName">
                    </fieldset>
                    <fieldset class="column is-half">
                        <label for="inpEmail">Email</label>
                        <input type="text" name="inpEmail" id="inpEmail">
                    </fieldset>
                    <fieldset class="column is-half">
                        <label for="inpNumber">Teléfono</label>
                        <input type="text" name="inpNumber" id="inpNumber">
                    </fieldset>
                    <fieldset class="column is-half">
                        <label for="inpInteresting">Estoy interesado en</label>
                        <select name="inpInteresting" id="inpInteresting">
                            <option value="Diseno Web">Diseño Web</option>
                            <option value="Paginas Web" selected="selected">Páginas Web</option>
                            <option value="Aplicaciones">Aplicaciones Móviles</option>
                            <option value="Software">Desarrollo de Software</option>
                            <option value="Otro">Otros</option>
                        </select>
                    </fieldset>
                    <fieldset class="column is-half">
                        <label for="inpKnowus">Como supiste de nosotros</label>
                        <select name="inpKnowus" id="inpKnowus">
                            <option value="Google">Google</option>
                            <option value="Facebook">Facebook</option>
                            <option value="Campaign">Anuncio</option>
                            <option value="Friend">Un amigo</option>
                            <option value="Otro">Otros</option>
                        </select>
                    </fieldset>
                    <fieldset class="column is-full">
                        <label for="inpComments">Describe brevemente tu proyecto</label>
                        <textarea name="inpComments" id="inpComments" rows="5"></textarea>
                    </fieldset>
                    <fieldset class="column is-full">
                        <button id="btnSendGlobalForm">CONTACTAR <i class="fas fa-arrow-right"></i></button>
                    </fieldset>
                </form>
            </div>

            <div class="column is-half">
                <img src="<?=_IMG.'contacto-il.png';?>">
            </div>

        </div>
    </div>
    
</section>

<?php require __DIR__ . '/components/footer.php'; ?>