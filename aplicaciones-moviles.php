<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-servicedetail">
    
    <div class="container">
        <div class="columns">

            <div class="column is-half">
                <h1><span>Aplicaciones</span> Móviles</h1>
                <div class="is-breads">
                    <?php include __DIR__ . '/components/services-breads.php'; ?>
                    <span>Aplicaciones Móviles</span>
                </div>

                <p class="is-pr-medium">Te ofrecemos el <strong>desarrollo de aplicaciones móviles en Cancún</strong>, en los sistemas operativos iOS y Android para tabletas y smartphones, y otros móviles.</p>
                <p class="is-pr-medium">Nos apasiona el mundo digital y las alternativas que surgen día a día para resolver las necesidades de tu negocio y ayudarte a alcanzar tus objetivos. Es por eso que nos especializamos en el <strong>desarrollo de aplicaciones móviles en Cancún</strong>, ya sean profesionales, de ocio, educativas, de acceso a servicios, o de algún segmento que tú requieras.</p>
                <p class="is-pr-medium">Al desarrollar una aplicación móvil referente a tu marca, obtendrás un mayor alcance con tus clientes, lograrás brindar un mejor servicio, y contarás con un respaldo frente a tu competencia.</p>
                <p class="is-pr-medium">Nuestro propósito es innovar e incrementar la productividad de tu empresa.</p>
                <p class="is-pr-medium">¡Si ya tienes una idea en mente, déjala en nuestras manos!</p>
            </div>
            
            <div class="column is-half">
                <img src="<?=_IMG.'apps-il.png';?>" class="is-img">
            </div>

        </div>

        <div class="columns is-multiline is-sub-services">

            <div class="column is-one-quarter">
                <div class="is-item">
                    <h3><i class="fas fa-copyright"></i> Identidad</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                </div>
            </div>

            <div class="column is-one-quarter">
                <div class="is-item">
                    <h3><i class="fas fa-bezier-curve"></i> Ilustración</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                </div>
            </div>

            <div class="column is-one-quarter">
                <div class="is-item">
                    <h3><i class="fas fa-print"></i> Para impresión</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                </div>
            </div>

            <div class="column is-one-quarter">
                <div class="is-item">
                    <h3><i class="fas fa-paint-brush"></i> Otros</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                </div>
            </div>

        </div>

    </div>
    

    <div class="is-steps">
        <div class="container">
            <div class="columns">

                <div class="column is-one-third">
                    <h3>¿Cómo trabajamos?</h3>
                    <p class="is-pr-big">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    <img src="<?=_IMG.'proceso-il.png';?>">
                </div>

                <div class="column is-two-thirds">
                    <div class="columns is-multiline">
                
                        <div class="column is-half">
                            <div class="is-item">
                                <h4>1. Enserio te escuchamos.</h4>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>
                        </div>

                        <div class="column is-half">
                            <div class="is-item">
                                <h4>2. Te estudiamos a detalle.</h4>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>
                        </div>

                        <div class="column is-half">
                            <div class="is-item">
                                <h4>3. Nos ponemos creativos.</h4>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>
                        </div>

                        <div class="column is-half">
                            <div class="is-item">
                                <h4>4. Tus propuestas.</h4>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>   
                        </div>

                        <div class="column is-half">
                            <div class="is-item">
                                <h4>5. Deleita a tus clientes.</h4>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <?php require __DIR__ . '/components/contact-service.php'; ?>
    
</section>

<?php require __DIR__ . '/components/footer.php'; ?>