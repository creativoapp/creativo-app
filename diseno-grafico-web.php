<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-servicedetail">
    
    <div class="container">
        <div class="columns">

            <div class="column is-half">
                <h1><span>Diseño</span> Gráfico</h1>
                <div class="is-breads">
                    <?php include __DIR__ . '/components/services-breads.php'; ?>
                    <span>Diseño Gráfico</span>
                </div>

                <p class="is-pr-medium">Ponemos a tu servicio nuestra <strong>agencia de diseño gráfico en Cancún</strong>.</p>
                <p class="is-pr-medium">Asegúrate de que tus futuros clientes tengan la mejor primera impresión de ti. Te ayudamos a alcanzar tu segmento de interés a través de un <strong>diseño gráfico</strong> enfocado específicamente en tus necesidades.</p>
                <p class="is-pr-medium">Transmite el propósito de tu marca y genera confianza y credibilidad en tus clientes por medio de un <strong>diseño gráfico</strong> que muestre una imagen profesional en cada uno de tus materiales, desde tu logotipo, imagen corporativa, materiales publicitarios o editoriales, sitio web, y más.</p>
                <p class="is-pr-medium">Estamos interesados en conocer más acerca de tu proyecto y en lograr que te distingas de la competencia.</p>
            </div>
            
            <div class="column is-half">
                <img src="<?=_IMG.'diseno-grafico-il.png';?>" class="is-img">
            </div>

        </div>

        <div class="columns is-multiline is-sub-services">

            <div class="column is-one-quarter">
                <div class="is-item">
                    <h3><i class="fas fa-copyright"></i> Identidad</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                </div>
            </div>

            <div class="column is-one-quarter">
                <div class="is-item">
                    <h3><i class="fas fa-bezier-curve"></i> Ilustración</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                </div>
            </div>

            <div class="column is-one-quarter">
                <div class="is-item">
                    <h3><i class="fas fa-print"></i> Para impresión</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                </div>
            </div>

            <div class="column is-one-quarter">
                <div class="is-item">
                    <h3><i class="fas fa-paint-brush"></i> Otros</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                </div>
            </div>

        </div>

    </div>
    

    <div class="is-steps">
        <div class="container">
            <div class="columns">

                <div class="column is-one-third">
                    <h3>¿Cómo trabajamos?</h3>
                    <p class="is-pr-big">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    <img src="<?=_IMG.'proceso-il.png';?>">
                </div>

                <div class="column is-two-thirds">
                    <div class="columns is-multiline">
                
                        <div class="column is-half">
                            <div class="is-item">
                                <h4>1. Enserio te escuchamos.</h4>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>
                        </div>

                        <div class="column is-half">
                            <div class="is-item">
                                <h4>2. Te estudiamos a detalle.</h4>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>
                        </div>

                        <div class="column is-half">
                            <div class="is-item">
                                <h4>3. Nos ponemos creativos.</h4>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>
                        </div>

                        <div class="column is-half">
                            <div class="is-item">
                                <h4>4. Tus propuestas.</h4>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>   
                        </div>

                        <div class="column is-half">
                            <div class="is-item">
                                <h4>5. Deleita a tus clientes.</h4>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <?php require __DIR__ . '/components/contact-service.php'; ?>
    
</section>

<?php require __DIR__ . '/components/footer.php'; ?>