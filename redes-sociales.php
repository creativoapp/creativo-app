<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-servicedetail">
    
    <div class="container">
        <div class="columns">

            <div class="column is-half">
                <h1><span>Redes</span> Sociales</h1>
                <div class="is-breads">
                    <?php include __DIR__ . '/components/services-breads.php'; ?>
                    <span>Redes Sociales</span>
                </div>

                <p class="is-pr-medium">Nuestros servicios de <strong>marketing en redes sociales</strong> se enfocan en llevar a tus clientes a la conversión.</p>
                <p class="is-pr-medium">A partir de un análisis detallado de tu negocio, establecemos la estrategia adecuada para tu <strong>marketing en redes sociales</strong>, creamos tus contenidos y gestionamos tus campañas dándoles un seguimiento puntual, con el que obtendrás reportes que te ayudarán a evolucionar con base en las necesidades de tu marca y de tus clientes.</p>
                <p class="is-pr-medium">Al adoptar una estrategia de <strong>marketing en redes sociales</strong> podrás conseguir una presencia activa que refuerce tu marca, segmentar el mercado al que te quieres dirigir, fortalecer el vínculo con tus clientes, y obtener credibilidad por parte de tus prospectos, además de brindarles una atención inmediata y personalizada.</p>
                <p class="is-pr-medium">Además, nuestro <strong>marketing en redes sociales</strong> complementará otras áreas digitales que también impactan a tu marca, como el tráfico y posicionamiento de tu sitio web, la amplificación de los contenidos de tu blog, el envío de mailing y newsletters, y mucho más.</p>
                <p class="is-pr-medium">¡Contáctanos y pongamos manos a la obra!</p>
            </div>
            
            <div class="column is-half">
                <img src="<?=_IMG.'redes-il.png';?>" class="is-img">
            </div>

        </div>

        <div class="columns is-multiline is-sub-services">

            <div class="column is-one-quarter">
                <div class="is-item">
                    <h3><i class="fas fa-copyright"></i> Identidad</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                </div>
            </div>

            <div class="column is-one-quarter">
                <div class="is-item">
                    <h3><i class="fas fa-bezier-curve"></i> Ilustración</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                </div>
            </div>

            <div class="column is-one-quarter">
                <div class="is-item">
                    <h3><i class="fas fa-print"></i> Para impresión</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                </div>
            </div>

            <div class="column is-one-quarter">
                <div class="is-item">
                    <h3><i class="fas fa-paint-brush"></i> Otros</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                </div>
            </div>

        </div>

    </div>
    

    <div class="is-steps">
        <div class="container">
            <div class="columns">

                <div class="column is-one-third">
                    <h3>¿Cómo trabajamos?</h3>
                    <p class="is-pr-big">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    <img src="<?=_IMG.'proceso-il.png';?>">
                </div>

                <div class="column is-two-thirds">
                    <div class="columns is-multiline">
                
                        <div class="column is-half">
                            <div class="is-item">
                                <h4>1. Enserio te escuchamos.</h4>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>
                        </div>

                        <div class="column is-half">
                            <div class="is-item">
                                <h4>2. Te estudiamos a detalle.</h4>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>
                        </div>

                        <div class="column is-half">
                            <div class="is-item">
                                <h4>3. Nos ponemos creativos.</h4>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>
                        </div>

                        <div class="column is-half">
                            <div class="is-item">
                                <h4>4. Tus propuestas.</h4>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>   
                        </div>

                        <div class="column is-half">
                            <div class="is-item">
                                <h4>5. Deleita a tus clientes.</h4>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <?php require __DIR__ . '/components/contact-service.php'; ?>
    
</section>

<?php require __DIR__ . '/components/footer.php'; ?>