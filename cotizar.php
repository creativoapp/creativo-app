<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-quote">
    <div class="container">
        <div class="columns">

            <div class="column is-two-thirds is-full-desktop is-full-touch is-full-tablet is-full-mobile">
                <h1>Cotiza tu proyecto</h1>
                <p class="is-pr-big">Estamos seguros que trabajar juntos será una grata experiencia, estamos ansiosos de trabajar contigo pide tu cotización sin compromiso.</p>
            </div>

        </div>
    </div>


    <div class="container is-form">
        <form name="frmQuote" id="frmQuote" action="" method="post" class="columns is-multiline"> 

              
            <div class="column is-full is-options">

                <div class="columns is-multiline">
                    <div class="column is-full"><h4>1. Elige un servicio</h4></div>
                    
                    <div class="column is-one-fifth is-item">
                        <div data-service="Design">
                            <i class="fas fa-palette"></i>   
                            <strong><span>Diseño</span> Web</strong>
                        </div>
                    </div>

                    <div class="column is-one-fifth is-item">
                        <div data-service="Web">
                            <i class="fas fa-laptop-code"></i>  
                            <strong><span>Páginas</span> Web</strong>
                        </div>
                    </div>

                    <div class="column is-one-fifth is-item">
                        <div data-service="Apps">
                            <i class="fas fa-mobile-alt"></i>   
                            <strong><span>Aplicaciones</span> Móviles</strong>
                        </div>
                    </div>

                    <div class="column is-one-fifth is-item">
                        <div data-service="Redes">
                            <i class="fab fa-uncharted"></i> 
                            <strong><span>Desarrollo de</span> Software</strong>
                        </div>
                    </div> 
                </div>

            </div>

            <div class="column is-two-thirds is-full-desktop is-full-touch is-full-tablet is-full-mobile">
                <h4>2. Cuéntanos de tu ti</h4>

                <div class="columns is-multiline">
                    <fieldset class="column is-two-thirds">
                        <label>Nombre</label>
                        <input type="text" name="inpName" id="inpName" >
                    </fieldset>
                    <fieldset class="column is-one-third">
                        <label>Email</label>
                        <input type="text" name="inpEmail" id="inpEmail" >
                    </fieldset>
                    <fieldset class="column is-one-third">
                        <label>Telefono</label>
                        <input type="text" name="inpNumberPhone" id="inpNumberPhone" >
                    </fieldset>
                    <fieldset class="column is-one-third">
                        <label>¿Dónde estas?</label>
                        <select name="inpState" id="inpState">
                            <option value="QR" selected="selected">Quintana Roo</option>
                            <option value="AS">Aguascalientes</option>
                            <option value="BC">Baja California</option>
                            <option value="BS">Baja California Sur</option>
                            <option value="CC">Campeche</option>
                            <option value="CS">Chiapas</option>
                            <option value="CH">Chihuahua</option>
                            <option value="DF">Ciudad de México</option>
                            <option value="CL">Coahuila</option>
                            <option value="CM">Colima</option>
                            <option value="DG">Durango</option>
                            <option value="MC">Estado de México</option>
                            <option value="GT">Guanajuato</option>
                            <option value="GR">Guerrero</option>
                            <option value="HG">Hidalgo</option>
                            <option value="JC">Jalisco</option>
                            <option value="MN">Michoacán</option>
                            <option value="MS">Morelos</option>
                            <option value="NT">Nayarit</option>
                            <option value="NL">Nuevo León</option>
                            <option value="OC">Oaxaca</option>
                            <option value="PL">Puebla</option>
                            <option value="QO">Querétaro</option>
                            <option value="SP">San Luis Potosí</option>
                            <option value="SL">Sinaloa</option>
                            <option value="SR">Sonora</option>
                            <option value="TC">Tabasco</option>
                            <option value="TS">Tamaulipas</option>
                            <option value="TL">Tlaxcala</option>
                            <option value="VZ">Veracruz</option>
                            <option value="YN">Yucatán</option>
                            <option value="ZS">Zacatecas</option>
                        </select>
                    </fieldset>
                    <fieldset class="column is-one-third">
                        <label>Cargo en el proyecto</label>
                        <select name="inpRol" id="inpRol">
                            <option value="OWN">Propietario</option>
                            <option value="JEF">Director de Área</option>
                        </select>
                    </fieldset>
                </div>

                <h4 class="is-mt-50">3. Cuéntanos de tu proyecto</h4>
                <div class="columns is-multiline">
                    <fieldset class="column is-one-third">
                        <label>Estado de tu proyecto</label>
                        <select name="inpProjectStatus" id="inpProjectStatus">
                            <option value="NEW">Quiero empezar</option>
                            <option value="OPT">Esta hecho pero quiero mejorarlo</option>
                        </select>
                    </fieldset>
                    <fieldset class="column is-one-third">
                        <label>Presupuesto</label>
                        <select name="inpProjectStatus" id="inpProjectStatus">
                            <option value="ABR">Abierto</option>
                            <option value="JST">Justo</option>
                            <option value="CLS">Bastante limitado</option>
                            <option value="NON">Sin presupuesto</option>
                        </select>
                    </fieldset>
                    <fieldset class="column is-one-third">
                        <label>Urgencia</label>
                        <select name="inpProjectStatus" id="inpProjectStatus">
                            <option value="JST">Ninguna, el tiempo necesario</option>
                            <option value="MOD">Moderada, el tiempo justo</option>
                            <option value="FAS">Importante, lo más pronto posible</option>
                        </select>
                    </fieldset>
                    <fieldset class="column is-full">
                        <label>Cuéntanos un poco más</label>
                        <textarea name="inpDetails" id="inpDetails" rows="5"></textarea>
                    </fieldset>
                    <fieldset class="column is-full">
                        <button id="btnQuoteForm">COTIZAR <i class="fas fa-paper-plane"></i></button>
                    </fieldset>
                </div>

            </div>

        </form>
    </div>
    
</section>

<?php require __DIR__ . '/components/footer.php'; ?>