<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-servicedetail">
    
    <div class="container">
        <div class="columns">

            <div class="column is-half">
                <h1><span>Páginas</span> Web</h1>
                <div class="is-breads">
                    <?php include __DIR__ . '/components/services-breads.php'; ?>
                    <span>Desarrollo de Páginas Web</span>
                </div>

                <p class="is-pr-medium">Nuestros servicios de <strong>desarrollo web en Cancún</strong> van desde la planificación, el diseño, la implementación y el mantenimiento de tu página web.</p>
                <p class="is-pr-medium">Partimos del objetivo de tu proyecto y lo transportamos a una página web en la que plasmamos la identidad de tu marca, a través de un diseño gráfico personalizado, y favorecemos la experiencia del usuario por medio de una interfaz práctica y amigable, tomando en cuenta aspectos de navegabilidad, interactividad, y arquitectura de la información.</p>
                <p class="is-pr medium">Nos aseguramos de que el desarrollo web de tu sitio cuente con técnicas y optimizaciones de vanguardia, de modo en que tus clientes puedan acceder a él y visualizarlo de manera óptima desde diferentes dispositivos, como computadoras, smartphones o tabletas.</p>
                <p class="is-pr medium">Además, le damos especial cuidado a tu contenido con el objetivo de brindar a los usuarios de tu página web aquello que están buscando, a través de medios como texto, imagen, audio, video y la optimización de motores de búsqueda.</p>
            </div>
            
            <div class="column is-half">
                <img src="<?=_IMG.'web-il.png';?>" class="is-img">
            </div>

        </div>

        <div class="columns is-multiline is-sub-services">

            <div class="column is-one-quarter">
                <div class="is-item">
                    <h3><i class="fas fa-shopping-cart"></i> Tiendas</h3>
                    <p>Vende tus productos, controla tu stock, cobra en linea y mucho más con esta solución hecha a la medida según tus necesidades y objetivos.</p>
                </div>
            </div>

            <div class="column is-one-quarter">
                <div class="is-item">
                    <h3><i class="fas fa-code"></i> A la medida</h3>
                    <p>Controla recursos, automatiza procesos; Desarrollamos a la medida justo lo que necesitas sin modulos que no necesitas u otros que faltan.</p>
                </div>
            </div>

            <div class="column is-one-quarter">
                <div class="is-item">
                    <h3><i class="fas fa-laptop"></i> Web Informativa</h3>
                    <p>Una web informativa no tiene que ser fea o simple, tu página web debe ser creativa, interactiva, moderna y completamente funcional.</p>
                </div>
            </div>

            <div class="column is-one-quarter">
                <div class="is-item">
                    <h3><i class="fab fa-wordpress-simple"></i> Blogs</h3>
                    <p>Los blogs son excelentes complementos para potencializar el impacto de tu web, o simplemente la mejor forma de contar tus historias.</p>
                </div>
            </div>

        </div>

    </div>
    

    <div class="is-steps">
        <div class="container">
            <div class="columns">

                <div class="column is-one-third">
                    <h3>¿Cómo trabajamos?</h3>
                    <p class="is-pr-big">En Creativo APP nos tomamos enserio el proceso de desarrollo para cada uno de nuestros servicios, y en el <strong>desarrollo de páginas web</strong> nos destaca la atención al detalle de cada uno de nuestros clientes.</p>
                    <img src="<?=_IMG.'proceso-il.png';?>">
                </div>

                <div class="column is-two-thirds">
                    <div class="columns is-multiline">
                
                        <div class="column is-half">
                            <div class="is-item">
                                <h4>1. Enserio te escuchamos</h4>
                                <p>Nos tomamos enserio la etapa de escucharte, ya que es vital para desarrollar un servicio de cálidad entender a la perfección lo que estas buscando.</p>
                            </div>
                        </div>

                        <div class="column is-half">
                            <div class="is-item">
                                <h4>2. Análisis de verdad</h4>
                                <p>No, no bajamos o compramos plantillas, enserio estudiamos la mejor forma de desarrollar la solución que estas buscando y sobre todo que necesitas.</p>
                            </div>
                        </div>

                        <div class="column is-half">
                            <div class="is-item">
                                <h4>3. Estamos de acuerdo</h4>
                                <p>Te compartimos los análisis, estrategías y objetivos para que desde el principio sepas que deberías de obtener de nosotros después de haberte expresado.</p>
                            </div>
                        </div>

                        <div class="column is-half">
                            <div class="is-item">
                                <h4>4. Programamos</h4>
                                <p>Disfrutamos programar, escribimos y escribimos para convertir tu idea y necesidad en algo funcional. Estamos ansiosos de trabajar contigo.</p>
                            </div>   
                        </div>

                        <div class="column is-half">
                            <div class="is-item">
                                <h4>5. Pruebalo</h4>
                                <p>Terminamos de programar y estamos ansiosos de mostarte como luce lo que hemos desarrollado para ti, probemos que todo funciona como esperabas.</p>
                            </div>
                        </div>

                        <div class="column is-half">
                            <div class="is-item">
                                <h4>6. Exito</h4>
                                <p>A estas alturas es seguro que estas disfrutando tu desarrollo y que ya se encuentra al alcanse de tus clientes. Felicidades para todos.</p>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <?php require __DIR__ . '/components/contact-service.php'; ?>
    
</section>

<?php require __DIR__ . '/components/footer.php'; ?>