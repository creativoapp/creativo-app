$(document).ready(function(){

    $('.is-tabs-clients a').on('click', function(event){
        event.preventDefault();

        var service = '.is-' + $(this).attr('href');
        
        $('.is-tabs-clients .is-body .column').fadeOut('last');
        $('.is-tabs-clients .is-body '+service+'').fadeIn('last');

        $('.is-tabs-clients a').removeClass('is-pushed');
        $(this).addClass('is-pushed');

        console.log(service);

    });

});
