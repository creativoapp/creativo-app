$(document).ready(function(){

    var appLoad = {};

    //Nivo Slider :: Home
	$('#is-comp-slider').nivoSlider({
	    effect: 'fade',
	    animSpeed: 500,                 
	    pauseTime: 5000,
	    directionNav: false,
	    controlNav: true
	});

	//Carrousel :: Home
	$('.is-rating').owlCarousel({
		loop: true,
		margin: 20,
		nav: true,
		autoplay: true,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			1000:{
				items:3
			}
		}
	});

	$('.is-ratingg').owlCarousel({
		loop: true,
		margin: 20,
		nav: true,
		autoplay: true,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			1000:{
				items:2
			}
		}
	});

	//FAQS acorddeon
	$('.is-questions .is-answer').hide();
	$('.is-questions .is-default').show();
	$('.is-questions .is-question').on('click', function(){

		if($(this).siblings('.is-answer').hasClass('is-default')) { return false; }

		$('.is-questions .is-answer').stop(true).slideUp('last').removeClass('is-default');
		$(this).siblings('.is-answer').slideDown('last').addClass('is-default');

		$('.is-questions i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
		$(this).children('i').removeClass('fa-chevron-down').addClass('fa-chevron-up');

	});


	//Wowjs
	new WOW().init();

	//Mobile menu
	appLoad.width = $(document).width();
	if(appLoad.width < 769) {
		$('.is-header').removeClass('is-transparent').addClass('is-solid');
	}

	appLoad.mobile = false;
	$('#mobile-nav').on('click', function(){

		console.log(appLoad);

		if(appLoad.mobile == false) {
			$('.is-menu').removeClass('is-hidden-mobile');
			appLoad.mobile = true;
			return false;
		}

		if(appLoad.mobile == true) {
			$('.is-menu').addClass('is-hidden-mobile');
			appLoad.mobile = false;
			return false;
		}

		
	});


});