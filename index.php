<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view-home">
    <div class="is-comp-slider nivoSlider" id="is-comp-slider">
        <img src="<?=_IMG.'paginas-web-creativo-app.jpg'; ?>" alt="Páginas Web en Cancún" title="<?=$sliderCaptions['web'];?>">
        <img src="<?=_IMG.'diseno-web-creativo-app.jpg'; ?>" alt="Diseño Web en Cancún" title="<?=$sliderCaptions['diseno'];?>">    
        <img src="<?=_IMG.'desarrollo-software-creativo-app.jpg'; ?>" alt="Desarrollo de Software en Cancún" title="<?=$sliderCaptions['software'];?>">
        <img src="<?=_IMG.'mobile-apps-creativo-app.jpg'; ?>" alt="Aplicaciones Moviles en Cancún" title="<?=$sliderCaptions['apps'];?>">
    </div>

    <!--SERVICIOS-->
    <div class="hero-body">
    <div class="container">
        <div class="columns is-multiline">

            <div class="column is-one-third">
                <h1 class="is-title"><span>Software Studio</span> en Cancún</h1>
                <p class="is-pr-medium">En <strong>Creativo APP</strong> te ayudamos a entender pero sobre todo a aprovechar el <strong>Marketing Digital</strong> para que tu marca tenga el impacto que necesitas para privilegiar a tu mercado con tu presencia.</p>
                <p class="is-pr-medium">El <strong>Marketing Digital en Cancún</strong> tiene ya muchos años consolidado por lo que es un excelente camino para llegar a tus consumidores.</p>
                <p class="is-pr-medium">Creativp APP es un <strong>Software Studio</strong> donde podrás encontrar asesoría y estrategias para realizar el proyecto que tu marca necesita y a lograr todos los objetivos que deseen alcanzar.</p>
            </div>

            <div class="column is-two-thirds">
                <div class="columns is-multiline is-services">

                    <div class="column is-half is-card">
                        <div>
                            <h2><i class="fas fa-palette"></i> <a href="/diseno-web"><span>Diseño</span> Web</a></h2>
                            <p>Aplicar correctamente el <strong>diseño web</strong> en tu negocio le permitirá llegar de forma rápid y eficiente a tu consumidor objetivo.</p>
                        </div>
                    </div>

                    <div class="column is-half is-card">
                        <div>
                            <h2><i class="fas fa-laptop-code"></i> <a href="/paginas-web">Páginas Web</a></h2>
                            <p>Diseñamos y creamos <strong>páginas web</strong> creativas, intuitivas y optimizadas para que tu marca transmita tu producto y/o servicio con exito.</p>
                        </div>
                    </div>

                    <div class="column is-half is-card">
                        <div>
                            <h2><i class="fab fa-uncharted"></i> <a href="/software">Desarrollo de Software</a></h2>
                            <p>Los procesos automaticos llevan años siendo el presente, desarrolla <strong>software a la medida</strong> para eficientar tus procesos y brindar herramientas ágiles en tu negocio.</p>
                        </div>
                    </div>

                    <div class="column is-half is-card">
                        <div>
                            <h2><i class="fas fa-mobile-alt"></i> <a href="/apps">Apps Móviles</a></h2>
                            <p>Desarrollamos <strong>aplicaciones móviles</strong> para ese producto o servicio que deseas esté en las manos de tu consumidor objetivo.</p>
                        </div>
                    </div>

                    

                </div>
            </div>

        </div>
    </div>
    </div>

    <!--EXPERIENCES-->
    <div class="is-experiences">
        <div class="container">
            <div class="columns is-multiline">

                <div class="column is-half">
                    <img src="<?=_IMG.'projects/expertos-en-aventura-experiencia.png';?>">
                </div>
                <div class="column is-half">
                    <h3>Expertos en Aventura</h3>
                    <p class="is-pr-big">Expertos en Aventuras es uno de nuestros clientes con exito, al que se le desarrollo un e-commerce con el objetivo de permitirle a sus clientes de forma eficiente y moderna reservar sus servicios turísticos.</p>
                    <p class="is-pr-medium"><strong>¿Tu marca necesita vender en linea?</strong><br>En Creativo App te ayudamos a desarrollar las herramientas para alcanzar los objetivos de tu negocio.</p>
                </div>
    

            </div>
        </div>
    </div>

    <!--CONTACTO-->
    <div class="is-we-contact">
        <div class="container">
            <div class="columns is-multiline">

                <div class="column is-full">
                    <h4>¿Tienes un proyecto sin mucho presupuesto?</h4>
                    <p>Estamos seguros de que hay una forma de llevarlo a cabo, déjanos contactarte.</p>

                    <div class="is-custom">
                        <input type="text" id="h_inpEmail" class="is-first" placeholder="Escribe aquí tu correo">
                        <input type="text" id="h_inpNumber" placeholder="Escribe aquí tu telefono">
                        <button class="is-last"><i class="fas fa-fax"></i> CONTÁCTAME</button>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!--CLIENTES-->
    <div class="is-clients">
        <div class="container">
            <div class="columns is-multiline">

                <div class="column is-full">
                    <h3>Ellos ya confiraron en Nostros!</h3>
                </div>

                <div class="column is-item">
                    <img src="<?=_IMG.'clients/sun-and-sands.png';?>" class="is-img-big is-img-centered">
                </div>
                <div class="column is-item">
                    <img src="<?=_IMG.'clients/tulum-sian-kaan-tours.png';?>" class="is-img-big is-img-centered">
                </div>
                <div class="column is-item">
                    <img src="<?=_IMG.'clients/ruben-frias.png';?>" class="is-img-big is-img-centered">
                </div>
                <div class="column is-item">
                    <img src="<?=_IMG.'clients/pioneros-cancun.png';?>" class="is-img-big is-img-centered">
                </div>
                <div class="column is-item">
                    <img src="<?=_IMG.'clients/jp-contadores.png';?>" class="is-img-big is-img-centered">
                </div> 
                <div class="column is-item">
                    <img src="<?=_IMG.'clients/grupo-recove.png';?>" class="is-img-big is-img-centered">
                </div>                

            </div>


            <div class="column is-full">
                <div class="owl-carousel is-rating">

                    <div class="item">
                        <strong>Pioneros Cancún FC</strong>
                        <p>Nuestra página web es muy fresca, moderna y podemos administrarla con muchisima facilidad, sin duda son una excelente opción.</p>

                        <div class="rating">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>

                    <div class="item">
                        <strong>Bekare Transfer</strong>
                        <p>Contar con software a la medida para mi negocio es vital, y en Crativo App encontre la forma de convertir mis necesidades en software de calidad. Muy contento con CreativoAPP.</p>

                        <div class="rating">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>

                    <div class="item">
                        <strong>Tulum Siankaan Tours</strong>
                        <p>Evaluamos varias opciones con costos elevados y con funciones prediseñadas que no le servían a nuestro negocio, Creativo App nos dio esa mezcla perfecta entre valor y utilidad que todo que empieza necesita.</p>

                        <div class="rating">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div> 

                    <div class="item">
                        <strong>Vuela Viajero</strong>
                        <p>Llegamos buscando ayuda técnica por problemas de malware en nuestra página web y salimos muy contentos con el problema resuelto y hasta con nueva imagen.</p>

                        <div class="rating">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div> 

                    <div class="item">
                        <strong>Buffete Jurídico Frías</strong>
                        <p>Encontre justo lo que buscaba y necesitaba para mis circunstancias más el plus de saber que puedo crecer mi web tanto como nosotros deseemos.</p>

                        <div class="rating">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>
                
                </div>
            </div>

        </div>
    </div>


</section>

<?php require __DIR__ . '/components/footer.php'; ?>