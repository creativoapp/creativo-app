<?php

//Constants
define('_IMG', '/assets/img/');
define('_CSS', '/assets/css/');
define('_JS', '/assets/js/');

//Global Vars
$pageUri = basename($_SERVER['SCRIPT_NAME']);
$cv = '1.0.0';

//Dinamic Content
$sliderCaptions = [
    'web' => '
            <h3>Diseño <span>Gráfico</span></h3>
            <p>La imagen de tu marca es tu carta de presentación ante tus posibles clientes. Destaca entre la competencia con una identidad única y profesional.</p>',
    'diseno' => '
            <h3>Posicionamiento <span>Web</span></h3>
            <p>Logra que tu página web tenga mayor relevancia entre los resultados de búsqueda de tus clientes potenciales.</p>',
    'apps' => '
            <h3>Desarrollo de Páginas <span>Web</span></h3>
            <p>Creamos tu página web desde cero con la personalidad de tu marca y con la mejor experiencia de navegación, convirtiendo la visita de tu cliente en un retorno de inversión.</p>',
    'software' => '
            <h3>Apps <span>Móviles</span></h3>
            <p>Atrévete a innovar tu negocio y a impulsar su alcance a través de uno de los medios digitales de mayor uso en la actualidad.</p>'
    ];